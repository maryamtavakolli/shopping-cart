import React from 'react';
import './App.scss';
import { Navbar, Grid, Row, Col } from 'react-bootstrap';
import { LoginPage,RegisterPage,MainPage, ProductPage, Cart } from "./_pages";

import { Router, Route ,Switch } from "react-router-dom";
import {PrivateRoute} from './_routes/privateRoute';
import {history} from './_helpers/history';
import { connect } from "react-redux";
import { getproducts } from './_actions';



function App ({user}) {
    return <Router history={history}>
      <Switch>
        <PrivateRoute path="/" isAuthenticated={!!user} exact component={MainPage}/>
        <Route path="/register" exact component={RegisterPage}/>
        <Route path="/login" exact component={LoginPage}/>
        <Route path="/product" exact component={ProductPage}/>
      
        <Route path="/cart" exact component={Cart}/>
      </Switch>
    </Router>;
}
const mapDispatchToProps = dispatch => ({
  getproducts : products => dispatch(getproducts(products))
})

const mapStateToProps = state => {
  const {user} = state.authentication;
  return { user };
};



const ConnectedApp = connect(mapStateToProps,mapDispatchToProps)(App);

export default  ConnectedApp;
