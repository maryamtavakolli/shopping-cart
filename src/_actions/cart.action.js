
import * as types from "../_constants";

export const receiveProducts = products => ({
        type : types.RECEIVE_PRODUCTS ,
        products:products
    })

export const addToCart = productId => ({
    type : types.ADD_TO_CART,
    productId:productId
})

export const checkout = () => ({
    type : types.CHECKOUT_REQUEST
})