import { ADD_TO_CART, CHECKOUT_REQUEST } from "../_constants";


const initialState = {
    addedIds : [],
    quantityById : {}
}

const addedIds = (state = initialState.addedIds , action) => {
    if(state.indexOf(action.productId) !== -1) {
        return state;
    }
    return [...state , action.productId];
}

const quantityById = (state = initialState.quantityById , action ) => {
    const { productId } = action;
    return {
        ...state,
        [productId] : (state[productId] || 0) + 1
    }
}


 export const cart = (state = initialState, action) => {
     console.log("cart " + action.type)
    switch (action.type) {
        case CHECKOUT_REQUEST : 
            return initialState;
        case ADD_TO_CART:
            return {
                addedIds : addedIds(state.addedIds , action),
                quantityById : quantityById(state.quantityById , action)
            }
        default:
            return state;
            console.log(state);
    }
}

