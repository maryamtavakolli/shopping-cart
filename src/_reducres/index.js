
import { combineReducers } from "redux";
import { authentication } from "./authentication.reducer";
import {products} from "./products.reducer";
import {cart} from "./cart.reducer";
const rootReducer = combineReducers({
    authentication,
    products,
    cart
});

export default rootReducer;
