import React, { Component } from 'react';
import { connect } from "react-redux";
import { ProductPage } from "./products.page";
import { checkout } from '../_actions';
import {  withRouter,Link } from "react-router-dom";
import {getproducts} from "../_actions";
import {receiveProducts} from "../_actions";
class Cart extends Component {


//   componentDidMount() {
//     this.props.dispatch(receiveProducts());
// }

  render() {
    const { products ,total, checkout} = this.props;
  
    // let total=0;
    //  (
        // products.map(product => 
            // <ProductPage/>
        // )
        // <ProductPage />
    // )
   
       
    
    return (
      <div>
           <em>Please add some products to cart.</em>
         <h3>Your Cart</h3>
         {/* <div>{nodes}</div> */}
         <ProductPage />
        
         <p>Total: ${total}</p>
      {console.log(total)}
      {console.log(products)}
    

         <button 
        //  disabled={ hasProducts ? '' : 'disabled'}
          onClick={checkout}>
             Checkout
         </button>
      </div>
    )
 
  }
}


const getCartProducts = state => {

    return state.cart.addedIds.map(id => ({
        ...state.products[id],
        quantity : (state.cart.quantityById[id] || 0),
        // price:state.products[id].price
    }))
}

const getTotal = state => state.cart.addedIds.reduce((total , id) => total + state.products.price * (state.cart.quantityById[id] || 0) , 0).toFixed(2)


const mapStateToProps = state => ({
    products : getCartProducts(state),
    total : getTotal(state)
})

const mapDispatchToProps = dispatch => ({
    checkout : () => dispatch(checkout(),
    
    )
})

// export default connect(mapStateToProps,mapDispatchToProps)(Cart);

const ConnectedCartPage = withRouter(connect(mapStateToProps,mapDispatchToProps)(Cart));
export {ConnectedCartPage as Cart}