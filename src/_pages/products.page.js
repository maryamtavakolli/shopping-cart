import React,{Component} from "react";
import { connect } from "react-redux";
import { getproducts } from "../_actions";
// import {cartAction} from "../_actions";
import {  withRouter,Link } from "react-router-dom";
import { addToCart } from '../_actions';
import {Button} from "./button.page";
class ProductPage extends Component {
    
    componentDidMount() {
        this.props.dispatch(getproducts());
    }
   
    render() {
        const {products,request}=this.props;
  
        return (
        <>
           
           { request ? 'loading ...' : <div>
                {products && products.map(product =>
                    <div key={product.id}>
                    name : {product.name} ---$ 
                       {product.price}
                        {/* {product.description} */}
                        <Button key={product.id} onAddToCartClicked={() =>this.props.dispatch(addToCart(product.id))} />

                    </div>)}
            </div>
           }
        </>);
    }
}

const mapStateToProps = (state) => {
 
    return {
       
        products: state.products.products,
        request: state.products.request
        // error: state.products.error
    
        // state
    }
};
const mapDispatchToProps = dispatch => {
    return {
        addToCart(product) {
            dispatch(addToCart(product));
        },

        dispatch
    }
}

// export default connect(mapStateToProps)(ProductPage);
// export { ProductPage}

const ConnectedProductsPage = withRouter(connect(mapStateToProps,mapDispatchToProps)(ProductPage));
export {ConnectedProductsPage as ProductPage}
